<?php

namespace Drupal\watchdog_file\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;

/**
 * Redirects logging messages to a file.
 */
class File implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * A configuration object containing syslog settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Constructs a SysLog object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser) {
    $this->config = $config_factory->get('watchdog_file.settings');
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, \Stringable|string $message, array $context = []) : void {
    global $base_url;

    $severity = RfcLogLevel::getLevels();

    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

    $entry = strtr($this->config->get('format'), [
      '!base_url' => $base_url,
      '!timestamp' => $context['timestamp'],
      '!severity' => $severity[$level],
      '!type' => $context['channel'],
      '!ip' => $context['ip'],
      '!request_uri' => $context['request_uri'],
      '!referer' => $context['referer'],
      '!uid' => $context['uid'],
      '!link' => strip_tags($context['link']),
      '!message' => strip_tags($message),
    ]);

    $this->writeEntry($entry);
  }

  /**
   * Write a watchdog entry to the log file.
   */
  protected function writeEntry($entry) {
    $success = FALSE;

    if ($this->isWritable()) {
      $filename = $this->config->get('log_filename');
      $data = $entry;
      $success = file_put_contents($filename, $data . "\n", FILE_APPEND);
    }

    return $success;
  }

  /**
   * Check if we're allowed to write to the log file.
   *
   * @return boolean
   *   Returns TRUE if the given file is writable.
   *   Returns FALSE if the given file is not writable.
   */
  protected function isWritable() {
    $filename = $this->config->get('log_filename');
    if (empty($filename)) {
      return FALSE;
    }

    if (!file_exists($filename)) {
      // The defined log file doesn't exist yet. Try to create it.
      if (!touch($filename)) {
        return FALSE;
      }
    }

    return is_writable($filename);
  }

}
